<?php

declare(strict_types=1);

namespace Drupal\path_watcher\EventSubscriber;

use Drupal\path_watcher\PathWatcherManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Path watcher event subscriber.
 */
class PathWatcherSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the object.
   *
   * @param \Drupal\path_watcher\PathWatcherManagerInterface $pathWatcherManager
   *   The path watcher manager.
   */
  public function __construct(protected readonly PathWatcherManagerInterface $pathWatcherManager) {}

  /**
   * Kernel terminate event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   Terminate event.
   */
  public function onKernelTerminate(TerminateEvent $event): void {
    if ($this->pathWatcherManager->needSaveVisit($event)) {
      $this->pathWatcherManager->saveVisit($event->getRequest()->getPathInfo());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [KernelEvents::TERMINATE => ['onKernelTerminate']];
  }

}
