<?php

declare(strict_types=1);

namespace Drupal\path_watcher;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a visit entity type.
 */
interface VisitInterface extends ContentEntityInterface, EntityOwnerInterface {}
