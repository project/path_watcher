<?php

declare(strict_types=1);

namespace Drupal\path_watcher\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configure path watcher settings for this site.
 */
class PathWatcherSettingsForm extends ConfigFormBase {

  use AutowireTrait;

  /**
   * The module config name.
   */
  public const CONFIG_NAME = 'path_watcher.settings';

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'path_watcher_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::CONFIG_NAME)->get();
    $form['skip_admin_paths'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip admin paths'),
      '#default_value' => $config['skip_admin_paths'],
      '#description' => $this->t('Ignore visits to paths starting with "/admin" (those with the "_admin_route" route option).'),
    ];
    $skip_usernames = $config['skip_usernames'];
    $form['skip_usernames'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Skip usernames'),
      '#default_value' => $skip_usernames ? implode(PHP_EOL, $skip_usernames) : '',
      '#rows' => min(5, count($skip_usernames) + 1),
      '#description' => $this->t('One username per line. One username per line. Ignore path visits for users with these names.'),
    ];
    $roles = [];
    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $rid => $role) {
      $roles[$rid] = $role->label();
    }
    $skip_roles = $config['skip_roles'];
    $form['skip_roles_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Skip roles (@count)', [
        '@count' => count($skip_roles),
      ]),
      '#open' => FALSE,
    ];
    $form['skip_roles_container']['skip_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Skip roles'),
      '#title_display' => 'invisible',
      '#default_value' => $skip_roles,
      '#options' => $roles,
      '#description' => $this->t('Ignore path visits for users with these roles.'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#open' => FALSE,
    ];
    $form['advanced']['skip_xml_http_request'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip XML HTTP requests'),
      '#default_value' => $config['skip_xml_http_request'],
      '#description' => $this->t('Ignore visits with the "X-Requested-With: XMLHttpRequest" header (AJAX requests).'),
    ];
    $form['advanced']['methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Processed methods'),
      '#options' => [
        Request::METHOD_HEAD => Request::METHOD_HEAD,
        Request::METHOD_GET => Request::METHOD_GET,
        Request::METHOD_POST => Request::METHOD_POST,
        Request::METHOD_PUT => Request::METHOD_PUT,
        Request::METHOD_PATCH => Request::METHOD_PATCH,
        Request::METHOD_DELETE => Request::METHOD_DELETE,
        Request::METHOD_PURGE => Request::METHOD_PURGE,
        Request::METHOD_OPTIONS => Request::METHOD_OPTIONS,
        Request::METHOD_TRACE => Request::METHOD_TRACE,
        Request::METHOD_CONNECT => Request::METHOD_CONNECT,
      ],
      '#default_value' => array_keys($config['methods']),
      '#description' => $this->t('Leave empty to skip method check. Only selected methods will be processed.'),
    ];
    $form['advanced']['content_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content type filter (regex)'),
      '#default_value' => $config['content_type'],
      '#placeholder' => '#text/html#',
      '#description' => $this->t('Leave empty to skip content type check. Only requests with matching "Content-Type" header will be processed.'),
    ];
    $paths = $config['paths'];
    $form['advanced']['paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Path filters (regex)'),
      '#default_value' => $paths ? implode(PHP_EOL, $paths) : '',
      '#placeholder' => "#^/oauth/#\n#^/some-api/#",
      '#description' => $this->t('One regular expression per line. Paths matching any of these expressions will be excluded.'),
      '#rows' => max(min(5, count($paths) + 1), 2),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getErrors()) {
      return;
    }

    // Validate content type filter.
    $content_type = trim($form_state->getValue('content_type'));
    @preg_match($content_type, '');
    if (preg_last_error() !== PREG_NO_ERROR) {
      $form_state->setErrorByName('content_type', $this->t('Invalid regular expression: @error', [
        '@error' => preg_last_error_msg(),
      ]));
    }
    // Validate path filters.
    $paths = array_unique(array_filter(array_map('trim', explode(PHP_EOL, $form_state->getValue('paths')))));
    foreach ($paths as $path) {
      @preg_match($path, '');
      if (preg_last_error() !== PREG_NO_ERROR) {
        $form_state->setErrorByName('paths', $this->t('Regular expression "@path" is invalid: @error', [
          '@path' => $path,
          '@error' => preg_last_error_msg(),
        ]));
        break;
      }
    }
    // Set values if no errors.
    if (!$form_state->getErrors()) {
      $form_state
        ->setValue('content_type', $content_type)
        ->setValue('paths', $paths);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->cleanValues()->getValues();
    $values['skip_roles'] = Checkboxes::getCheckedCheckboxes($values['skip_roles']);
    $values['skip_usernames'] = array_unique(array_filter(array_map('trim', explode(PHP_EOL, $values['skip_usernames']))));
    $values['methods'] = array_fill_keys(Checkboxes::getCheckedCheckboxes($values['methods']), TRUE);

    $this->config(static::CONFIG_NAME)
      ->setData($values)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
