<?php

declare(strict_types=1);

namespace Drupal\path_watcher;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a path entity type.
 */
interface PathInterface extends ContentEntityInterface {}
