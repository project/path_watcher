<?php

declare(strict_types=1);

namespace Drupal\path_watcher;

use Symfony\Component\HttpKernel\Event\TerminateEvent;

/**
 * Provides an interface for a path watcher manager.
 */
interface PathWatcherManagerInterface {

  /**
   * Determine if the visit needs to be saved.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The terminate event.
   *
   * @return bool
   *   TRUE if the visit needs to be saved, FALSE otherwise.
   */
  public function needSaveVisit(TerminateEvent $event): bool;

  /**
   * Saves the visit for the current user.
   *
   * @param string $path
   *   The path.
   *
   * @return \Drupal\path_watcher\VisitInterface
   *   The saved visit.
   */
  public function saveVisit(string $path): VisitInterface;

  /**
   * Returns the hash of the path.
   *
   * @param string $path
   *   The path.
   *
   * @return string
   *   The hash of the path.
   */
  public function generatePathHash(string &$path): string;

}
