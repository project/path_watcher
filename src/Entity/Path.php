<?php

declare(strict_types=1);

namespace Drupal\path_watcher\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\path_watcher\PathInterface;

/**
 * Defines the path entity class.
 *
 * @ContentEntityType(
 *   id = "pw_path",
 *   label = @Translation("PW: path"),
 *   label_collection = @Translation("Paths"),
 *   label_singular = @Translation("path"),
 *   label_plural = @Translation("paths"),
 *   label_count = @PluralTranslation(
 *     singular = "@count paths",
 *     plural = "@count paths",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pw_path",
 *   admin_permission = "administer path_watcher",
 *   entity_keys = {
 *     "id" = "hash",
 *     "label" = "path_alias",
 *   },
 *   links = {},
 * )
 */
class Path extends ContentEntityBase implements PathInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Hash'))
      ->setRequired(TRUE)
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', 128)
      ->setSetting('case_sensitive', FALSE);
    $fields['path'] = BaseFieldDefinition::create('uri')
      ->setLabel(new TranslatableMarkup('Path'))
      ->setRequired(TRUE);
    $fields['path_alias'] = BaseFieldDefinition::create('uri')
      ->setLabel(new TranslatableMarkup('Path alias'))
      ->setRequired(TRUE);

    return $fields;
  }

}
