<?php

declare(strict_types=1);

namespace Drupal\path_watcher\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\path_watcher\VisitInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the visit entity class.
 *
 * @ContentEntityType(
 *   id = "pw_visit",
 *   label = @Translation("PW: visit"),
 *   label_collection = @Translation("Visits"),
 *   label_singular = @Translation("Visit"),
 *   label_plural = @Translation("Visits"),
 *   label_count = @PluralTranslation(
 *     singular = "@count visits",
 *     plural = "@count visits",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pw_visit",
 *   admin_permission = "administer path_watcher",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "path",
 *     "owner" = "uid",
 *   },
 *   links = {},
 * )
 */
class Visit extends ContentEntityBase implements VisitInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields['uid']->setLabel(new TranslatableMarkup('Authored by'))
      ->setRequired(TRUE);

    $fields['path'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Path'))
      ->setDescription(new TranslatableMarkup('The path entity that was visited.'))
      ->setSetting('target_type', 'pw_path')
      ->setRequired(TRUE);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Authored on'))
      ->setDescription(new TranslatableMarkup('The time that the visit was created.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
