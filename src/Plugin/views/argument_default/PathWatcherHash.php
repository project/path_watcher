<?php

declare(strict_types=1);

namespace Drupal\path_watcher\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\path_watcher\PathWatcherManagerInterface;
use Drupal\views\Attribute\ViewsArgumentDefault;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default argument plugin to extract the current path hash.
 */
#[ViewsArgumentDefault(
  id: 'path_watcher_hash',
  title: new TranslatableMarkup('Path watcher hash'),
)]
class PathWatcherHash extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * The path watcher manager.
   */
  protected PathWatcherManagerInterface $pathWatcherManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->pathWatcherManager = $container->get('path_watcher.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(): string {
    $path = $this->requestStack->getCurrentRequest()->getPathInfo();
    return $this->pathWatcherManager->generatePathHash($path);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return ['url.path'];
  }

}
