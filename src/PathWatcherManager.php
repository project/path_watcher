<?php

declare(strict_types=1);

namespace Drupal\path_watcher;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\path_watcher\Form\PathWatcherSettingsForm;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

/**
 * Manages the saving of visits.
 */
class PathWatcherManager implements PathWatcherManagerInterface {

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   The admin context.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The alias manager.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly AccountInterface $currentUser,
    protected readonly AdminContext $adminContext,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly AliasManagerInterface $aliasManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function needSaveVisit(TerminateEvent $event): bool {
    // Skip drush requests.
    if (PHP_SAPI === 'cli') {
      return FALSE;
    }
    // Skip non successful responses.
    $response = $event->getResponse();
    if (!$response->isSuccessful()) {
      return FALSE;
    }

    $config = $this->configFactory->get(PathWatcherSettingsForm::CONFIG_NAME)->get();
    if ($config['content_type'] && !preg_match($config['content_type'], $response->headers->get('Content-Type'))) {
      return FALSE;
    }
    $request = $event->getRequest();
    if ($config['skip_xml_http_request'] && $request->isXmlHttpRequest()) {
      return FALSE;
    }
    if ($config['methods'] && empty($config['methods'][$request->getMethod()])) {
      return FALSE;
    }
    if ($config['skip_admin_paths'] && $this->adminContext->isAdminRoute()) {
      return FALSE;
    }
    if ($config['skip_roles'] && array_intersect($this->currentUser->getRoles(), $config['skip_roles'])) {
      return FALSE;
    }
    if ($config['skip_usernames'] && in_array($this->currentUser->getAccountName(), $config['skip_usernames'])) {
      return FALSE;
    }
    if ($config['paths']) {
      $path_info = $request->getPathInfo();
      foreach ($config['paths'] as $path) {
        if (preg_match($path, $path_info)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function saveVisit(string $path): VisitInterface {
    $path_hash = $this->generatePathHash($path);
    $path_storage = $this->entityTypeManager->getStorage('pw_path');
    if (!$path_entity = $path_storage->load($path_hash)) {
      $path_entity = $path_storage->create([
        'hash' => $path_hash,
        'path' => $path,
        'path_alias' => $this->aliasManager->getAliasByPath($path),
      ]);
      $path_entity->save();
    }
    /** @var \Drupal\path_watcher\VisitInterface $visit */
    $visit = $this->entityTypeManager->getStorage('pw_visit')->create(['path' => $path_entity]);
    $visit->save();
    return $visit;
  }

  /**
   * {@inheritdoc}
   */
  public function generatePathHash(string &$path): string {
    $path = $this->aliasManager->getPathByAlias($path);
    return hash('xxh128', $path);
  }

}
