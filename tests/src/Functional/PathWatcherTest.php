<?php

declare(strict_types=1);

namespace Drupal\Tests\path_watcher\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\WaitTerminateTestTrait;
use Drupal\path_watcher\Form\PathWatcherSettingsForm;

/**
 * @covers \Drupal\path_watcher\PathWatcherManager
 * @covers \Drupal\path_watcher\EventSubscriber\PathWatcherSubscriber
 */
class PathWatcherTest extends BrowserTestBase {

  use WaitTerminateTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'views', 'path_watcher'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the path watcher.
   */
  public function testPathWatcher(): void {
    $admin = $this->createUser(admin: TRUE);
    $this->config(PathWatcherSettingsForm::CONFIG_NAME)->set('skip_usernames', [$admin->getAccountName()])->save();

    $user = $this->createUser();
    $cases = [
      [
        'path' => '/admin',
        'visit_count' => 1,
        'entity_count' => 0,
        'user' => $admin,
      ],
      [
        'path' => '/node',
        'visit_count' => 2,
        'entity_count' => 2,
        'user' => $user,
      ],
      [
        'path' => '/' . $this->drupalCreateNode(['type' => $this->drupalCreateContentType()->id()])->toUrl()->getInternalPath(),
        'visit_count' => 3,
        'entity_count' => 3,
        'user' => $user,
      ],
      [
        'path' => '/node',
        'visit_count' => 4,
        'entity_count' => 0,
        'user' => NULL,
      ],
    ];

    $this->setWaitForTerminate();
    $path_watcher_manager = $this->container->get('path_watcher.manager');
    $visit_storage = $this->container->get('entity_type.manager')->getStorage('pw_visit');
    foreach ($cases as $case) {
      if ($user = $case['user']) {
        $this->drupalLogin($user);
      }

      for ($i = 0; $i < $case['visit_count']; $i++) {
        $this->drupalGet($case['path']);
        $this->assertSession()->statusCodeEquals(200);
      }

      $entity_count = $visit_storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('path', $path_watcher_manager->generatePathHash($case['path']))
        ->condition('uid', $user?->id() ?? 0)
        ->count()
        ->execute();
      $this->assertEquals($case['entity_count'], $entity_count);

      if ($user) {
        $this->drupalLogout();
      }
    }
  }

}
