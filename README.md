# Path watcher

This module provides the ability to collect path browsing statistics.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. Go to settings at `/admin/config/system/path-watcher`.
2. Fill out the appropriate form and save.
3. Use views to show statistics.
